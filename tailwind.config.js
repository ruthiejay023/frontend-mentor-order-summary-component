/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    colors: {
      'pale-blue': 'hsl(225, 100%, 94%)',
      'bright-blue': 'hsl(245, 75%, 52%)',
      'very-pale-blue': 'hsl(225, 100%, 98%)',
      'desaturated-blue': 'hsl(224, 23%, 55%)',
      'dark-blue': 'hsl(223, 47%, 23%)',
      'blue': 'hsla(245, 83%, 68%, 1)',
      'white': 'hsl(0, 100%, 100%)'
    },
    fontFamily: {
      RedHeartDisplay: ['Red Hat Display', 'cursive']
    },
  },
  plugins: [],
}

